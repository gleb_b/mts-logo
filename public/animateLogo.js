
var duration = 1200;

var s = Snap('#MTSlogo');


var outlineGrad = s.select('#linearGradient4751');
var signal = s.select('#signal1');
var maskLogo = s.select('#maskLogo');
var maskPulse = s.select('#maskPulse');
var pulse = s.select('#pulse'); 
var logoOutline = s.select('#logoOutline'); 
var reversedGrad = s.select('#reversedGradient'); 
var sensors = s.select('#sensors'); 
var logoShade = s.select('#logoShade');
var circleLeft = s.select('#circleLeft');
var circleRight = s.select('#circleRight');

function animGrad () { 


  outlineGrad.animate({ x2: 100 },duration*1, mina.easeout) ;
} 




function animSignal () { 
  signal.attr({ opacity:1, transform:'translateX(-74)' });
  signal.animate({ transform:'translateX(90)' },duration*1, mina.easeout) ;         

} 

function leftBounce () {  
  circleLeft.animate({ opacity:0, r:8 },duration*0.4, mina.easeout) ;         

} 

function rightBounce () {  
  circleRight.animate({ opacity:0, r:8 },duration*0.4, mina.easeout) ;         

} 

function fadeLogo() { maskLogo.attr({ transform:'translateX(-130)' });
maskLogo.animate({ transform:'translateX(140)' },duration*1.1, mina.easeout) ; }



function pulseDraw (){ 
  var lineLength = Snap.path.getTotalLength(pulse);  
  pulse.attr({
    fill:'none',
    opacity:1, 
    'stroke-dasharray': lineLength + ' ' + lineLength,
    'stroke-dashoffset': lineLength 
  });
  pulse.animate({
    strokeDashoffset : 0
  },duration*0.8, mina.easeout) ;
}           


function fadePulse() {
 maskPulse.animate({ transform:'translateX(-115)' },duration*0.65, mina.easeout) ;
 setTimeout(function(){ pulse.animate({ opacity:0 },duration*0.1, mina.easeout) ; },duration*0.55); 


}


function fadeLogoIn() { 
  document.getElementById('pulse-layer').style.opacity = 0;
  document.getElementById('logoFill').style.opacity = 1;
  maskLogo.attr({ transform:'translateX(-130)' });
  maskLogo.animate({ transform:'translateX(0)' },duration*0.6, mina.easeout) ; }

  function glareLogo () { 

    logoOutline.attr({ fill:'url(#reversedGradient)'  });
    
    reversedGrad.animate({   x2:100 },duration*0.6, mina.easeout) ; 
    
  } 

  function shadeLogo () {  

    logoShade.attr({ opacity:1  });
    logoShade.animate({ transform:'skewX(30) translateX(-35)',  opacity:0 },duration*0.5, mina.easeout) ;        
  } 


  function showSensors () { 

    sensors.attr({ transform:'translate(0, 8)'  });

    sensors.animate({  transform:'translate(0)',  opacity:1 },duration*0.3, mina.easeout) ; 

  } 



  document.getElementById('sensors').style.opacity = 0;
  document.getElementById('logoFill').style.opacity = 0;


  setTimeout(function(){ document.getElementById('logoOutlineLayer').style.opacity = 1;  },duration*0.5); 

  setTimeout(function(){ animSignal();  },duration*0.2); 
  setTimeout(function(){ animGrad(); },duration*0.3); 
  setTimeout(function(){ fadeLogo(); },duration*0.3);


  setTimeout(function(){ leftBounce();  },duration*0.93); 
  setTimeout(function(){ pulseDraw(); },duration*0.95);      
  setTimeout(function(){ fadePulse(); },duration*1.35); 
  setTimeout(function(){ rightBounce();  },duration*1.75); 

  setTimeout(function(){ animSignal();  },duration*1.75);  
  setTimeout(function(){  fadeLogoIn();  },duration*2); 
  setTimeout(function(){  glareLogo();  },duration*2);  
  setTimeout(function(){  shadeLogo();  },duration*2);  
  setTimeout(function(){  showSensors();  },duration*2.4); 



